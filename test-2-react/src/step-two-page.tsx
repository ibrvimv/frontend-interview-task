import { useState } from 'react'
import { useHistory } from 'react-router'

export default function StepTwoPage(): JSX.Element {
    const [email] = useState<string>(localStorage.getItem('savedEmail') || '')
    const history = useHistory()

    const handleGoBack = () => {
        history.goBack()
    }
    const handleConfirm = async (): Promise<void> => {
        try {
            const response = await fetch('http://localhost:4040/endpoint', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ email }),
            })

            if (response.ok) {
                alert('success!')
            } else {
                alert('something went wrong!')
            }
        } catch (error) {
            console.error('Error:', error)
            console.log('error!')
        }
    }

    return (
        <div className="flex flex-col justify-between h-full">
            <p>Email: {email}</p>
            <div className="flex w-full justify-between  gap-4">
                <button onClick={handleGoBack} className={`btn btn-primary flex-1`}>
                    Back
                </button>
                <button onClick={handleConfirm} className={`btn btn-primary flex-1`}>
                    Confirm
                </button>
            </div>
        </div>
    )
}
