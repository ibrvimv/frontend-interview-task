import { Redirect, Route } from 'react-router'
import { BrowserRouter } from 'react-router-dom'
import StepTwoPage from './step-two-page'
import LoginPage from './step-one-page'
export default function App() {
    return (
        <BrowserRouter>
            <header className="h-20 bg-primary flex items-center p-4">
                <h1 className="text-3xl text-black">Title</h1>
            </header>
            <main className="flex flex-col p-4 h-full">
                <Redirect exact from="/" to="/login/step-1" />
                <Route path="/login/step-1" component={LoginPage} />
                <Route path="/login/step-2" component={StepTwoPage} />
            </main>
        </BrowserRouter>
    )
}
