import { useState, useRef, useEffect } from 'react'
import { Route, Switch, useHistory } from 'react-router'
import FormCheckbox from './components/form-checkbox'
import FormInput from './components/form-input'

export default function LoginPage() {
    const history = useHistory()
    const [email, setEmail] = useState<string>('')
    const [isChecked, setIsChecked] = useState<boolean>(false)
    const holdTimer = useRef<NodeJS.Timeout | null>(null)
    const buttonRef = useRef<HTMLButtonElement>(null)
    const counter = useRef<number>(500)
    const isEmailValid = (input: string): boolean => {
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(input)
    }

    const handleEmailChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        setEmail(e.target.value)
    }

    const handleCheckboxChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        setIsChecked(e.target.checked)
    }

    const handleHoldStart = (): void => {
        if (isEmailValid(email) && isChecked) {
            console.log('start')

            holdTimer.current = setInterval(() => {
                if (holdTimer.current && counter.current <= 0) {
                    clearInterval(holdTimer.current)
                    history.push('/login/step-2')
                } else counter.current -= 100

                if (buttonRef.current) {
                    buttonRef.current.innerText = `Hold to proceed ${counter.current}ms`
                }
            }, 100)
        }
    }
    const handleHoldRelease = (): void => {
        console.log('stop')
        if (holdTimer.current) clearInterval(holdTimer.current)
        counter.current = 500
        if (buttonRef.current) {
            buttonRef.current.innerText = `Hold to proceed 500ms`
        }
    }

    useEffect(() => {
        const storedEmail = localStorage.getItem('savedEmail')
        if (storedEmail) {
            setEmail(storedEmail)
        }
    }, [])

    useEffect(() => {
        localStorage.setItem('savedEmail', email)
    }, [email])

    return (
        <Switch>
            <Route>
                <FormInput value={email} onChange={handleEmailChange} />
                <div className="p-1"></div>
                <FormCheckbox checked={isChecked} onChange={handleCheckboxChange} />
                <button
                    ref={buttonRef}
                    disabled={!isEmailValid(email) || !isChecked}
                    onMouseDown={handleHoldStart}
                    onMouseUp={handleHoldRelease}
                    onMouseLeave={handleHoldRelease}
                    className={`btn btn-primary mt-auto`}
                >
                    Hold to proceed 500ms
                </button>
            </Route>
            <Route>Not implemented</Route>
        </Switch>
    )
}
