interface FormInputProps {
    value: string
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}
export default function FormInput({ value, onChange }: FormInputProps): JSX.Element {
    return (
        <label className="form-control">
            <div className="label">
                <span className="label-text">Email</span>
            </div>
            <input type="email" placeholder="Type here" className="input" value={value} onChange={onChange} />
            {/* <div className="label">
                <span className="label-text-alt">Helper text</span>
            </div> */}
        </label>
    )
}
