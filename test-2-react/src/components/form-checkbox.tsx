interface FormCheckboxProps {
    checked: boolean
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}

export default function FormCheckbox({ checked, onChange }: FormCheckboxProps): JSX.Element {
    return (
        <div className="form-control">
            <label className="label cursor-pointer justify-start gap-2">
                <input type="checkbox" className="checkbox checkbox-primary" checked={checked} onChange={onChange} />
                <span className="label-text">I agree</span>
            </label>
        </div>
    )
}
